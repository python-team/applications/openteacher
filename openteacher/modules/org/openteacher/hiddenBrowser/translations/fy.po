# Frisian translation for openteacher
# Copyright (c) 2012 Rosetta Contributors and Canonical Ltd 2012
# This file is distributed under the same license as the openteacher package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2012.
#
msgid ""
msgstr ""
"Project-Id-Version: openteacher\n"
"Report-Msgid-Bugs-To: openteachermaintainers@lists.launchpad.net\n"
"POT-Creation-Date: 2013-06-27 15:37+0200\n"
"PO-Revision-Date: 2012-10-03 14:54+0000\n"
"Last-Translator: Marten de Vries <marten@marten-de-vries.nl>\n"
"Language-Team: Frisian <fy@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2013-06-04 05:44+0000\n"
"X-Generator: Launchpad (build 16660)\n"

#: hiddenBrowser.py:87
msgid "Hide the browser!"
msgstr "Ferbergje de browser!"

#: hiddenBrowser.py:88
msgid "Hide the others; make space for the browser"
msgstr "Ferbergje de oare dingen; meitsje plak foar de browser"

#: hiddenBrowser.py:252
msgid "Enable hidden browser (hide it by moving the slider)"
msgstr "Set the ferburgen browser oan (ferbergje it troch de slider te bewege)"
