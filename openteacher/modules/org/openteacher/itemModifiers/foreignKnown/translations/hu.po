# Hungarian translation for openteacher
# Copyright (c) 2011 Rosetta Contributors and Canonical Ltd 2011
# This file is distributed under the same license as the openteacher package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2011.
#
msgid ""
msgstr ""
"Project-Id-Version: openteacher\n"
"Report-Msgid-Bugs-To: openteachermaintainers@lists.launchpad.net\n"
"POT-Creation-Date: 2013-06-27 15:37+0200\n"
"PO-Revision-Date: 2011-09-13 07:44+0000\n"
"Last-Translator: Richard Somlói <ricsipontaz@gmail.com>\n"
"Language-Team: Hungarian <hu@li.org>\n"
"Language: hu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2013-06-04 05:42+0000\n"
"X-Generator: Launchpad (build 16660)\n"

#. TRANSLATORS: This is an option. When checked, OT asks the
#. TRANSLATORS: answers and expects from you to give the
#. TRANSLATORS: corresponding questions.
#: foreignKnown.py:54
msgid "Ask foreign - known"
msgstr ""

#~ msgid "Foreign - Known"
#~ msgstr "Idegen – Ismert"
