# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: openteachermaintainers@lists.launchpad.net\n"
"POT-Creation-Date: 2013-06-27 15:37+0200\n"
"PO-Revision-Date: 2012-04-17 07:55+0000\n"
"Last-Translator: Marten de Vries <marten@marten-de-vries.nl>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2013-06-04 05:42+0000\n"
"X-Generator: Launchpad (build 16660)\n"

#: about.py:43
msgid "Project website"
msgstr "Projectwebsite"

#: about.py:93
msgid "Full license text"
msgstr "Volledige licentietekst"

#: about.py:199
msgid "Thanks to all Launchpad contributors!"
msgstr "Alle helpers op Launchpad, bedankt!"

#: about.py:244 about.py:245
msgid "About"
msgstr "Over"

#: about.py:246
msgid "License"
msgstr "Licentie"

#: about.py:247
msgid "Authors"
msgstr "Auteurs"

#~ msgid "About module"
#~ msgstr "Overmodule"

#~ msgid "OpenTeacher authors"
#~ msgstr "OpenTeacher auteurs"
