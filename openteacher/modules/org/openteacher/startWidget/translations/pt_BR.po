# Brazilian Portuguese translation for openteacher
# Copyright (c) 2012 Rosetta Contributors and Canonical Ltd 2012
# This file is distributed under the same license as the openteacher package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2012.
#
msgid ""
msgstr ""
"Project-Id-Version: openteacher\n"
"Report-Msgid-Bugs-To: openteachermaintainers@lists.launchpad.net\n"
"POT-Creation-Date: 2013-06-27 15:37+0200\n"
"PO-Revision-Date: 2013-06-02 22:51+0000\n"
"Last-Translator: Marcelo Thomaz <marcelotfilho@gmail.com>\n"
"Language-Team: Brazilian Portuguese <pt_BR@li.org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2013-06-04 05:43+0000\n"
"X-Generator: Launchpad (build 16660)\n"

#: startWidget.py:248
msgid "Create lesson:"
msgstr "Criar lição:"

#: startWidget.py:249
msgid "Load lesson:"
msgstr "Carregar lição:"

#: startWidget.py:250
msgid "Load from the internet"
msgstr "Carregar a partir da Internet"

#: startWidget.py:252
msgid "Recently opened:"
msgstr "Abertos recentemente:"
