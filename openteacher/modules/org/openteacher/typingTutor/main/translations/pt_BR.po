# Brazilian Portuguese translation for openteacher
# Copyright (c) 2012 Rosetta Contributors and Canonical Ltd 2012
# This file is distributed under the same license as the openteacher package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2012.
#
msgid ""
msgstr ""
"Project-Id-Version: openteacher\n"
"Report-Msgid-Bugs-To: openteachermaintainers@lists.launchpad.net\n"
"POT-Creation-Date: 2013-06-27 15:37+0200\n"
"PO-Revision-Date: 2013-02-02 12:39+0000\n"
"Last-Translator: Adriano Steffler <Unknown>\n"
"Language-Team: Brazilian Portuguese <pt_BR@li.org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2013-06-04 05:44+0000\n"
"X-Generator: Launchpad (build 16660)\n"

#: main.py:102
msgid "That's a mistake :(."
msgstr "Isto é um erro :-("

#: main.py:144
msgid "Please choose a user name and the keyboard layout you want to learn."
msgstr ""
"Por favor, escolha um nome de usuário e o leiaute do teclado que você deseja "
"aprender."

#: main.py:145
msgid "Username:"
msgstr "Nome de usuário:"

#: main.py:146
msgid "Keyboard layout:"
msgstr "Leiaute do teclado:"

#. TRANSLATORS: Split the line as closest to the middle as
#. TRANSLATORS: possible, not necessarily between 'Keyboard
#. TRANSLATORS: layout' and 'preview'. Thanks :)
#: main.py:150
msgid ""
"Keyboard layout\n"
"preview:"
msgstr ""
"Prévia do leiaute\n"
"do teclado:"

#: main.py:205
msgid "Welcome, please choose your account by clicking on it."
msgstr "Bem-vindo! Por favor, escolha a sua conta clicando nela."

#: main.py:206
msgid "I am a new user"
msgstr "Eu sou um novo usuário"

#: main.py:275
msgid "Level"
msgstr "Nível"

#: main.py:276
msgid "Speed (words per minute)"
msgstr "Velocidade (palavras por minuto)"

#: main.py:277
msgid "Amount of mistakes"
msgstr "Quantidade de erros"

#: main.py:278
msgid "Start exercise"
msgstr "Começar o exercício"

#: main.py:329
msgid "Username empty"
msgstr "Nome de usuário vazio"

#: main.py:330
msgid "The username should not be empty. Please try again."
msgstr "O nome de usuário não pode estar vazio. Por favor, tente novamente."

#: main.py:336
msgid "Username taken"
msgstr "Nome de usuário utilizado"

#: main.py:337
msgid "That username is already taken. Please try again."
msgstr ""
"Este nome de usuário já está sendo utilizado. Por favor, tente novamente."

#: main.py:438 main.py:448
msgid "Typing Tutor"
msgstr "Tutor de datilografia"
