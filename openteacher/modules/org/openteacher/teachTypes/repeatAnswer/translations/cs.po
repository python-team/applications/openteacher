# Czech translation for openteacher
# Copyright (c) 2012 Rosetta Contributors and Canonical Ltd 2012
# This file is distributed under the same license as the openteacher package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2012.
#
msgid ""
msgstr ""
"Project-Id-Version: openteacher\n"
"Report-Msgid-Bugs-To: openteachermaintainers@lists.launchpad.net\n"
"POT-Creation-Date: 2013-06-27 15:37+0200\n"
"PO-Revision-Date: 2012-11-04 13:52+0000\n"
"Last-Translator: Jan Žárský <jan.zarsky@gmail.com>\n"
"Language-Team: Czech <cs@li.org>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2013-06-04 05:42+0000\n"
"X-Generator: Launchpad (build 16660)\n"

#: repeatAnswer.py:72
msgid "Click the button to start"
msgstr "Klikněte pro spuštění"

#: repeatAnswer.py:73
msgid "Start!"
msgstr "Spustit!"

#: repeatAnswer.py:186
msgid "Repeat answer"
msgstr "Zopakovat odpověď"

#: repeatAnswer.py:190
msgid "Repeat mode fade duration (milliseconds)"
msgstr "Délka zobrazení opakování (milisekundy)"

#: repeatAnswer.py:191
msgid "Lesson"
msgstr "Lekce"

#: repeatAnswer.py:192
msgid "Words lesson"
msgstr ""
