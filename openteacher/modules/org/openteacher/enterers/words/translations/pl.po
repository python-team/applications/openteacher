# Polish translation for openteacher
# Copyright (c) 2013 Rosetta Contributors and Canonical Ltd 2013
# This file is distributed under the same license as the openteacher package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2013.
#
msgid ""
msgstr ""
"Project-Id-Version: openteacher\n"
"Report-Msgid-Bugs-To: openteachermaintainers@lists.launchpad.net\n"
"POT-Creation-Date: 2013-06-27 15:37+0200\n"
"PO-Revision-Date: 2013-06-04 00:19+0000\n"
"Last-Translator: pp/bs <Unknown>\n"
"Language-Team: Polish <pl@li.org>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2013-06-05 05:45+0000\n"
"X-Generator: Launchpad (build 16660)\n"

#: words.py:233
msgid "Questions"
msgstr "Pytania"

#: words.py:233
msgid "Answers"
msgstr "Odpowiedzi"

#: words.py:233
msgid "Comment"
msgstr "Komentarz"

#: words.py:233
msgid "Comment after answering"
msgstr "Komentarz po udzieleniu odpowiedzi"

#: words.py:475
msgid "Title:"
msgstr "Tytuł:"

#: words.py:476
msgid "Question language:"
msgstr "Język pytania:"

#: words.py:477
msgid "Answer language:"
msgstr "Język odpowiedzi"

#: words.py:478
msgid "Remove selected row(s)"
msgstr "Usuń zaznaczone rzędy"
