# Brazilian Portuguese translation for openteacher
# Copyright (c) 2012 Rosetta Contributors and Canonical Ltd 2012
# This file is distributed under the same license as the openteacher package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2012.
#
msgid ""
msgstr ""
"Project-Id-Version: openteacher\n"
"Report-Msgid-Bugs-To: openteachermaintainers@lists.launchpad.net\n"
"POT-Creation-Date: 2013-06-27 15:37+0200\n"
"PO-Revision-Date: 2012-09-18 00:53+0000\n"
"Last-Translator: Adriano Steffler <Unknown>\n"
"Language-Team: Brazilian Portuguese <pt_BR@li.org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2013-06-04 05:43+0000\n"
"X-Generator: Launchpad (build 16660)\n"

#: media.py:173
msgid "Title:"
msgstr ""

#: media.py:175
msgid "Remove"
msgstr "Remover"

#: media.py:176
msgid "Add local media"
msgstr "Adicionar mídia local"

#: media.py:177
msgid "Add remote media"
msgstr "Adicionar mídia remota"

#: media.py:179
msgid "Name:"
msgstr "Nome:"

#: media.py:180
msgid "Question:"
msgstr "Pergunta:"

#: media.py:181
msgid "Answer:"
msgstr "Resposta:"

#: media.py:199
msgid "Select file(s)"
msgstr "Selecionar arquivo(s)"

#: media.py:201
msgid "Media"
msgstr "Mídia"

#: media.py:222
msgid "File URL"
msgstr "URL do arquivo"

#: media.py:223
msgid ""
"Enter the URL of your website or media item.\n"
"Supported video sites: "
msgstr ""
"Digite a URL do seu sítio ou item de mídia.\n"
"Sítios de vídeo suportados: "

#: media.py:274
msgid "Unsupported file type"
msgstr "Tipo de arquivo não suportado"

#: media.py:275
msgid "This type of file is not supported:\n"
msgstr "Este tipo de arquivo não é suportado:\n"
