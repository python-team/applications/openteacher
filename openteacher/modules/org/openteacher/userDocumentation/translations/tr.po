# Turkish translation for openteacher
# Copyright (c) 2013 Rosetta Contributors and Canonical Ltd 2013
# This file is distributed under the same license as the openteacher package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2013.
#
msgid ""
msgstr ""
"Project-Id-Version: openteacher\n"
"Report-Msgid-Bugs-To: openteachermaintainers@lists.launchpad.net\n"
"POT-Creation-Date: 2013-06-27 15:38+0200\n"
"PO-Revision-Date: 2013-04-29 10:54+0000\n"
"Last-Translator: kodadiirem <Unknown>\n"
"Language-Team: Turkish <tr@li.org>\n"
"Language: tr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2013-06-04 05:44+0000\n"
"X-Generator: Launchpad (build 16660)\n"

#: getting-started.html:2
msgid "Using OpenTeacher 3: A Quick Start"
msgstr "OpenTeacher 3'ü Kullanırken: Hızlı Bir Başlangıç"

#: getting-started.html:6
msgid ""
"This quick start guide shows you how to do a simple self-test using "
"OpenTeacher. We're going to learn counting to ten in French today! For this "
"example we are assuming that you already know English, and you want to learn "
"French."
msgstr ""
"Bu hızlı başlangıç kılavuzu size kendinizi OpenTeacher ile nasıl test "
"edeceğinizi gösterir. Bugün Fransızcada 10'a kadar saymayı öğreneceğiz! Bu "
"örnekte sizin İngilizce bildiğinizi ve Fransızca öğrenmek istediğinizi "
"varsayıyoruz."

#: getting-started.html:10
msgid ""
"The screenshots are all taken in Ubuntu, but this should all work exactly "
"the same in Windows and Mac OS X."
msgstr ""
"Ekran görüntüleri Ubuntu'da çekilmiştir fakat Windows ve Mac OS X'te de "
"yaklaşık olarak aynı şekilde çalışır."

#: getting-started.html:14
msgid ""
"Start OpenTeacher and click the 'Create words lesson' button to make a new "
"word lesson."
msgstr ""
"OpenTeacher'i başlatın ve yeni bir kelimeler dersi oluşturmak için "
"'Kelimeler dersi oluştur' düğmesini tıklayın."

#: getting-started.html:19
msgid ""
"Enter a title for the word list you are going to enter. Also enter the known "
"language, which, in our case, is English, and the foreign language (the "
"language you want to learn), which, in our case, is French."
msgstr ""
"Giriş yapmak istediğiniz kelimeler listesi için bir başlık giriniz. "
"Bildiğiniz bir dil ve bilmediğiniz (öğrenmek istediğiniz) bir dil giriniz."

#: getting-started.html:21
msgid ""
"Then enter all words you want to learn in the words table, you can go to the "
"next question or answer column by pressing tab or '='. See the screenshot "
"for an example."
msgstr ""
"Daha sonra kelimeler tablosundaki öğrenmek istediğiniz tüm kelimeleri "
"giriniz, sekmeye veya '='tıklayarak bir sonraki soruya gidebilirsiniz. Bir "
"örnek için ekran görüntüsüne bakınız."

#: getting-started.html:26
msgid ""
"Click the 'Teach me!' tab on the bottom to start the test. You can now "
"specify in what way you want OpenTeacher to question you, but the defaults "
"will do in our case. So, we click the 'I'm ready, start the lesson!' button."
msgstr ""
"Teste başlamak için alttaki 'Öğret bana!' sekmesine tıklayınız. Şimdi "
"OpenTeacher'in size soracağı soruları özelleştirebilirsiniz, biz şimdi "
"varsayılan olanları kullanacağız. Bu yüzden 'Hazırım, ders başlasın!' "
"düğmesine tıkladık."

#: getting-started.html:28
msgid ""
"A word will now be asked to you in your own language, and you'll have to "
"type the translation of it in the text field. When you have entered the "
"translation, press Enter on your keyboard, or click 'Check!'"
msgstr ""
"Sizin dilinizde bir kelime sorulacaktır ve kelime giriş alanında o kelimenin "
"çevirisini girmek zorundasınız. Çeviriyi girdiğinizde, 'Kontrol et!' "
"düğmesine ya da klavyenizden Enter tuşuna basın."

#: getting-started.html:32
msgid ""
"If you entered the right translation, the next question will be asked to "
"you. If you did not enter the right translation, OpenTeacher will show you "
"the correct answer, and the difference from your answer."
msgstr ""
"Eğer doğru çeviriyi girdiyseniz, size bir sonraki soru sorulacak. Eğer doğru "
"çeviriyi giremediyseniz, OpenTeacher size doğru çeviriyi ve sizin "
"çevirinizle arasındaki farkı gösterecektir."

#: getting-started.html:37
msgid ""
"Once the test is completed, OpenTeacher will tell you if you did well by "
"giving you a note."
msgstr "Bir kere testi bitirdiğinizde OpenTeacher size bir not verecektir."
