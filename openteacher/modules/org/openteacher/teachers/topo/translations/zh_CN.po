# Chinese (Simplified) translation for openteacher
# Copyright (c) 2013 Rosetta Contributors and Canonical Ltd 2013
# This file is distributed under the same license as the openteacher package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2013.
#
msgid ""
msgstr ""
"Project-Id-Version: openteacher\n"
"Report-Msgid-Bugs-To: openteachermaintainers@lists.launchpad.net\n"
"POT-Creation-Date: 2013-06-27 15:37+0200\n"
"PO-Revision-Date: 2013-06-20 02:15+0000\n"
"Last-Translator: adam liu <xhiyua@gmail.com>\n"
"Language-Team: Chinese (Simplified) <zh_CN@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2013-06-21 05:43+0000\n"
"X-Generator: Launchpad (build 16677)\n"

#: topo.py:92
msgid "Place - Name"
msgstr "地点 - 名称"

#: topo.py:93
msgid "Name - Place"
msgstr "名称 - 地点"

#: topo.py:167
msgid "Lesson type:"
msgstr "课程形式:"

#: topo.py:168
msgid "Lesson order:"
msgstr "课程顺序:"

#: topo.py:169
msgid "Which place is here?"
msgstr "这是什么地方?"

#. TRANSLATORS: A button the user clicks to let the computer check the given answer.
#: topo.py:171
msgid "Check"
msgstr "核对"

#: topo.py:172
msgid "Please click this place:"
msgstr "请点击这个地点:"

#: topo.py:323
msgid "Please click this place: "
msgstr "请点击这个地点: "
