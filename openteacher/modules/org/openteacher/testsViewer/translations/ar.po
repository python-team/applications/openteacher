# Arabic translation for openteacher
# Copyright (c) 2011 Rosetta Contributors and Canonical Ltd 2011
# This file is distributed under the same license as the openteacher package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2011.
#
msgid ""
msgstr ""
"Project-Id-Version: openteacher\n"
"Report-Msgid-Bugs-To: openteachermaintainers@lists.launchpad.net\n"
"POT-Creation-Date: 2013-06-27 15:37+0200\n"
"PO-Revision-Date: 2011-10-11 08:32+0000\n"
"Last-Translator: El Achèche ANIS <elachecheanis@gmail.com>\n"
"Language-Team: Arabic <ar@li.org>\n"
"Language: ar\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2013-06-04 05:43+0000\n"
"X-Generator: Launchpad (build 16660)\n"

#: testsViewer.py:51
msgid "Date"
msgstr "التّاريخ"

#: testsViewer.py:52
msgid "Note"
msgstr "ملاحظة"

#: testsViewer.py:53
msgid "Completed"
msgstr "مكتمل"

#: testsViewer.py:118
msgid "Highest note:"
msgstr "أعلى درجة"

#: testsViewer.py:121
msgid "Average note:"
msgstr "الدرجة المتوسّطة"

#: testsViewer.py:124
msgid "Lowest note:"
msgstr "أقلّ درجة"

#. TRANSLATORS: This is meant as 'here would normally
#. stand a note, but not today.' If '-' isn't
#. appropriate in you language for that, please replace.
#. Otherwise, just copy the original.
#: testsViewer.py:140 testsViewer.py:145 testsViewer.py:149 testsViewer.py:173
#: testsViewer.py:179
msgid "-"
msgstr "-"

#: testsViewer.py:247
msgid "Back"
msgstr "إلى الخلف"
